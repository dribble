/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

enum position { Goalie, Defender, Midfielder, Attacker };
const char *position_name[4] = { "Goalie", "Defender", "Midfielder", "Attacker" };

struct player
{
	int number;
	int age;
	int skill;
	int wage;
	int pos;
	const char *name;
};

static int el;
static int pnum = 0;
static struct player *pvec;

int player_init(int elements)
{
	el = elements;
	pvec = calloc(elements, sizeof(*pvec));

	if(pvec == NULL)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int player_generate(int country, int player_index, int division_order_number)
{
	struct player *p;

	p = &pvec[pnum];
	p->name = country_random_player_name(country);
	p->number = player_index+2;
	p->age = 17 + (rand() % 17);
	p->skill = (rand() % 25) - (division_order_number) + 60 - (rand() % (34 - p->age));
	p->wage = (p->skill*1200)-(rand()%1500);
	if(player_index < 2+rand()%2)
	{
		p->pos = Goalie;
	}
	else if(player_index < 9+rand()%2)
	{
		p->pos = Defender;
	}
	else if(player_index >= 9 && player_index<15+rand()%2)
	{
		p->pos = Midfielder;
	}
	else if(player_index >= 15)
	{
		p->pos = Attacker;
	}

	return pnum++;
}

void player_print(int player)
{
	struct player *p;
	p = &pvec[player];
	printf("%3d. %-15s \t Skill: %-3d \t Age: %-3d \t Wage: %-9d \t Position: %-10s\n",
			p->number,
			p->name,
			p->skill,
			p->age,
			p->wage,
			position_name[p->pos]);
}

void player_free()
{
	free(pvec);
}

#ifdef FTEST_PLAYER
#include <assert.h>
#include <time.h>
int main(int argc, char *argv[])
{
	int italy;
	int p;
	srand(time(NULL));
	italy = country_read(DATADIR "italy.txt");
	assert(player_init(2) == 0);
	p = player_generate(italy, 0, 0);
	assert(p == 0);
	p = player_generate(italy, 1, 0);
	assert(p == 1);
	assert(pvec[p].number == 3);
	player_free();
	country_free();
	return 0;
}
#endif 

