/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

struct team
{
	const char *name;
	int *players;
	int cash;
	int numplayers;
};

static int tnum = 0;
static struct team *tvec;

int team_init(int elements)
{
	tvec = calloc(elements, sizeof(*tvec));

	if(tvec == NULL)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int team_generate(int country, int division_rank, int division_order, int player_max)
{
	struct team *t;

	t = &tvec[tnum];
	t->players = calloc(player_max, sizeof(*t->players));

	for(t->numplayers=0 ; t->numplayers<player_max; t->numplayers++)
	{
		t->players[t->numplayers] = player_generate(country, t->numplayers, division_order);
	}

	t->cash = 0;
	t->name = country_team_name_in_division(country, division_rank, tnum);

	return tnum++;
}

void team_free()
{
	int i;

	for(i=0 ; i<tnum; i++)
	{
		free(tvec[i].players);
	}

	free(tvec);
}

#ifdef FTEST_TEAM
#include <assert.h>
#include <time.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int italy;
	int nteams;
	int nplayers;
	int t;

	srand(time(NULL));

	italy = country_read(DATADIR "italy.txt");
	nteams = country_num_teams_total(italy);
	nplayers = 20+20+25;

	assert(player_init(nplayers) == 0);
	assert(team_init(nteams) == 0);

	t = team_generate(italy, 1, 0, 20);
	assert(tvec[t].numplayers == 20);
	assert(strcmp(tvec[t].name, "Inter") == 0);

	t = team_generate(italy, 1, 1, 20);
	assert(tvec[t].numplayers == 20);
	assert(strcmp(tvec[t].name, "AS Roma") == 0);

	t = team_generate(italy, 2, 2, 25);
	assert(tvec[t].numplayers == 25);
	assert(strcmp(tvec[t].name, "Genoa") == 0);

	player_free();
	team_free();
	country_free();

	return 0;
}
#endif 

