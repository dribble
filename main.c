/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <time.h>
#include "defs.h"

int main(int argc, char *argv[])
{
	puts("dribble  Copyright (C) 2008  Jonas Sandberg")
	puts("This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.")
	puts("This is free software, and you are welcome to redistribute it")
	puts("under certain conditions; type `show c' for details.")

	srand(time(NULL));
	game_init();
	cli_main();
	game_free();
}

