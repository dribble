/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <readline/readline.h>
#include "defs.h"

static char inbuffer[128];

static void new_game()
{
	char *in;
	int ritems;

	in = readline("Your name, sir: ");
	sscanf(in, "%s", inbuffer);
	free(in);

	game_set_manager_name(inbuffer);

	/*
	 * 1. choose country
	 * 2. choose division
	 * 3. choose team
	 */
	in = readline("Please pick a division: \n");
	sscanf(in, "%d", inbuffer);
	free(in);

	for(i=0; i<NUM_TEAMS; i++)
	{
		printf("%d. %s\n", i+1, serie_a_team_names[i]);
	}

	for(ritems = 0 ; ritems == 0 ; )
	{
		in = readline("Select a team: ");
		ritems = sscanf(in, "%d", &div->managed_team);
		free(in);

		if(div->managed_team < 1 || div->managed_team > NUM_TEAMS)
		{
			ritems = 0;
		}
		else
		{
			div->managed_team--;
		}
	}
}

void cli_main()
{
	char *in;
	int ritems;
	int choice;

	puts("Dribble.");
	puts("");
	puts("1. New league");
	puts("2. Load league");
	puts("3. Exit");

	in = readline("[1]: ");
	ritems = sscanf(in, "%d", &choice);
	free(in);

	if(ritems != 1)
	{
		choice = 1;
	}

	switch(choice)
	{
	case 1:
		puts("New league.");
		new_game();
		break;
	case 2:
		puts("Load league.");
		break;
	case 3:
		puts("Exit.");
		break;
	}
}

