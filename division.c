/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "defs.h"

struct match
{
	int home;
	int away;
};

struct division
{
	int country;
	int division;
	int n_teams;
	int *teams;
	struct match **matches;
};

static int dnum = 0;
static struct division *dvec;

static void assign_matches(struct division *d)
{
	int total_rounds;
	int matches_per_round;
	int *c1;
	int *c2;
	int round;
	int mn;
	int i;

	total_rounds = d->n_teams-1;
	matches_per_round = d->n_teams/2;
	c1 = calloc(total_rounds, sizeof(*c1));
	c2 = calloc(total_rounds, sizeof(*c2));
	d->matches = malloc(sizeof(*d->matches)*total_rounds);

	for(i=0 ; i<total_rounds ; i++)
	{
		d->matches[i] = malloc(sizeof(**d->matches)*matches_per_round);
	}

	if(d->matches == NULL)
	{
		perror("malloc");
	}

	for(i=0; i<matches_per_round; i++)
	{
		c1[i] = i;
		c2[i] = total_rounds-i;
	}

	for(round=0 ; round<total_rounds; round++)
	{
		int last;

		for(mn=0; mn<matches_per_round; mn++)
		{
			if(mn == 0 && round%2 == 0)
			{
				d->matches[round][mn].home = c1[mn];
				d->matches[round][mn].away = c2[mn];
			}
			else if(mn == 0 && round%2 == 1)
			{
				d->matches[round][mn].home = c2[mn];
				d->matches[round][mn].away = c1[mn];
			}
			else if(round%2 == 0)
			{
				d->matches[round][mn].home = c1[mn];
				d->matches[round][mn].away = c2[mn];
			}
			else if(round%2 == 1)
			{
				d->matches[round][mn].home = c2[mn];
				d->matches[round][mn].away = c1[mn];
			}
		}

		/* Fixture shift */
		last = c1[matches_per_round-1];
		for(mn=matches_per_round-1 ; mn>0; mn--)
		{
			c1[mn] = c1[mn-1];
		}
		c1[0] = c2[1];
		c2[matches_per_round] = last;
		for(mn=1 ; mn<matches_per_round; mn++)
		{
			c2[mn] = c2[mn+1];
		}
	}

	for(round=0 ; round<total_rounds; round++)
	{
		for(mn=0; mn<matches_per_round; mn++)
		{
			int home;
			int away;
			home = d->matches[round][mn].home;
			away = d->matches[round][mn].away;
		}
	}

	free(c1);
	free(c2);
}

int division_init(int elements)
{
	dvec = calloc(elements, sizeof(*dvec));

	if(dvec == NULL)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int division_rand_players_base()
{
	return 18;
}

int division_rand_players_top()
{
	return 5;
}

int division_generate(int country, int division_rank)
{
	struct division *d;
	int i;

	d = &dvec[dnum];
	d->n_teams = country_num_teams_in_division(country, division_rank);
	assign_matches(d);
	d->teams = calloc(d->n_teams, sizeof(*d->teams));

	for(i=0 ; i<d->n_teams ; i++)
	{
		d->teams[i] = team_generate(country, division_rank, i, division_rand_players_base() + (rand() % division_rand_players_top()));
	}

	return dnum++;
}

int division_num_teams(int division)
{
	return dvec[division].n_teams;
}

void division_free()
{
	int i;
	int j;

	for(i=0; i<dnum; i++)
	{
		free(dvec[i].teams);

		for(j=0 ; j<dvec[i].n_teams-1 ; j++)
		{
			free(dvec[i].matches[j]);
		}

		free(dvec[i].matches);
	}

	free(dvec);
}

#ifdef FTEST_DIVISION
#include <assert.h>
#include <stdlib.h>
#include <time.h>
int main(int argc, char *argv[])
{
	int italy;
	int nteams;
	int ndivs;
	int serie_a;
	int serie_b;

	srand(time(NULL));

	italy = country_read(DATADIR "italy.txt");
	nteams = country_num_teams_total(italy);
	ndivs = country_num_divisions(italy);

	assert(player_init((division_rand_players_base()+division_rand_players_top())*nteams) == 0);
	assert(team_init(nteams) == 0);
	assert(division_init(ndivs) == 0);

	serie_a = division_generate(italy, 1);
	assert(division_num_teams(serie_a) == 20);
	serie_b = division_generate(italy, 2);
	assert(division_num_teams(serie_b) == 22);
	country_free();
	player_free();
	team_free();
	division_free();
	return 0;
}
#endif 
