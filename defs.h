/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/

int country_read(char *filename);
char *country_playername(int country, int pindex);
int country_num_playernames(int country);
int country_num_teams_in_division(int country, int division_rank);
int country_num_divisions(int country);
void country_free();
char *country_random_player_name(int country);
int country_num_teams_total(int country);
char *country_team_name_in_division(int country, int division_rank, int team_index);
int country_division_rank(int country, int division);



void cli_main();

void game_init();
void game_showteams();

int team_init(int elements);
int team_generate(int country, int division, int division_rank, int player_max);

int player_init(int elements);
int player_generate(int country, int player_index, int division_order_number);
void player_print(int player);

void player_free();
void team_free();

int division_generate(int country, int division_rank);
int division_rand_players_base();
int division_rand_players_top();
int division_init(int elements);
void division_free();

