/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string.h>
#include "defs.h"

struct game
{
	int version;
	char *notepad[256];
	int countrydatabase[1];
	char *manager;
	int m_country;
	int m_division;
	int m_team;
};

static struct game g;

void game_init()
{
	bzero(g, sizeof(*g));
	g.countrydatabase[0] = country_read("italy.txt");
}

void game_free()
{
	free(g.manager);
}

void game_set_manager_name(char *s)
{
	g.manager = strdup(s);
}

