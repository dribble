/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

struct league
{
	int ndivs;
	int *divisions;
	const char *name;
};

static int lnum = 0;
static struct league *lvec;

int league_init(int elements)
{
	lvec = calloc(elements, sizeof(*lvec));

	if(lvec == NULL)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int league_generate(int country, const char *name)
{
	int i;

	lvec[lnum].ndivs = country_num_divisions(country);
	lvec[lnum].divisions = calloc(lvec[lnum].ndivs, sizeof(*lvec[lnum].divisions));

	for(i=0 ; i<lvec[lnum].ndivs ; i++)
	{
		int rank;
		rank = country_division_rank(country, i);
		lvec[lnum].divisions[i] = division_generate(country, rank);
	}

	lvec[lnum].name = name;

	return lnum++;
}

int league_num_divs(int league)
{
	return lvec[league].ndivs;
}

void league_free()
{
	int i;
	for(i=0 ; i<lnum ; i++)
	{
		free(lvec[i].divisions);
	}
	free(lvec);
}

#ifdef FTEST_LEAGUE
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
int main(int argc, char *argv[])
{
	int italy;
	int nteams;
	int ndivs;
	int liga;

	srand(time(NULL));

	italy = country_read(DATADIR "italy.txt");
	nteams = country_num_teams_total(italy);
	ndivs = country_num_divisions(italy);

	assert(player_init((division_rand_players_base()+division_rand_players_top())*nteams) == 0);
	assert(team_init(nteams) == 0);
	assert(division_init(ndivs) == 0);
	assert(league_init(1) == 0);
	
	liga = league_generate(italy, "Italian league");
	assert(league_num_divs(liga) == 2);
	assert(strcmp(lvec[0].name, "Italian league") == 0);

	country_free();
	player_free();
	team_free();
	division_free();
	league_free();
	return 0;
}
#endif 
