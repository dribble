/*
	dribble - a football manager game
	Copyright (C) 2008  Jonas Sandberg

	This file is part of dribble.

	dribble is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	dribble is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with dribble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>

struct country_team_name
{
	char *name;
	int division_rank;
};

struct country
{
	char *filebuffer;
	int num_playernames;
	int num_divs;
	void **pvector;
	struct country_team_name *tvector;
	int tncount;
	void **dvector;
};

static int cnum = 0;
static struct country cvec[1];

static int check_allocations_exist(int country)
{
	if(cvec[country].tvector == NULL)
	{
		fprintf(stderr, "No team name allocations specified!\n");
		return -1;
	}
	else if(cvec[country].pvector == NULL)
	{
		fprintf(stderr, "No player name allocations specified!\n");
		return -1;
	}

	return 0;
}

int country_read(char *filename)
{
	FILE *fp;
	struct stat fileinfo;
	intmax_t filesize;
	size_t read_items;
	char *token;
	char mode;
	struct country *c;
	size_t tsize;
	size_t psize;
	size_t dsize;

	c = &cvec[cnum];
	bzero(c, sizeof(*c));

	fp = fopen(filename, "r");

	if(fp == NULL)
	{
		return -1;
	}

	if(stat(filename, &fileinfo) == -1)
	{
		return -1;
	}
	else
	{
		filesize = fileinfo.st_size;
	}

	c->filebuffer = (char *) calloc(1, filesize+1);
	if(c->filebuffer == NULL)
	{
		return -1;
	}

	read_items = fread(c->filebuffer, filesize, 1, fp);
	if(read_items != 1)
	{
		return -1;
	}

	if(fclose(fp) == EOF)
	{
		return -1;
	}

	token = strtok(c->filebuffer, "\n");
	if(token == NULL)
	{
		return -1;
	}

	for(mode=' '; (token = strtok(NULL, "\n")) != NULL;)
	{
		if(strcmp(token, "ALLOCATIONS") == 0)
		{
			mode = 'A';
			continue;
		}
		else if(strncmp(token, "TEAMS - ", 8) == 0)
		{
			mode = 'T';
			c->dvector[c->num_divs] = token + 8;
			if(c->num_divs == dsize)
			{
				fprintf(stderr, "Allocations and division count do not match!\n");
				return -1;
			}
			c->num_divs++;
			continue;
		}
		else if(strncmp(token, "NAMES", 5) == 0)
		{
			mode = 'P';
			continue;
		}

		switch(mode)
		{
			case 'A':
				if(sscanf((char *) token, "%u %u %u", &tsize, &psize, &dsize) != 3)
				{
					fprintf(stderr, "Missing allocation numbers on format <num> <num>\n");
					return -1;
				}
				else
				{
					c->dvector = calloc(dsize, sizeof(*c->dvector));
					c->pvector = calloc(psize, sizeof(*c->pvector));
					c->tvector = calloc(tsize, sizeof(*c->tvector));
				}
				break;
			case 'T':
				if(check_allocations_exist(cnum) == -1)
				{
					return -1;
				}
				if(c->tncount == tsize)
				{
					fprintf(stderr, "Allocations and team count does not match!\n");
					return -1;
				}
				c->tvector[c->tncount].name = token;
				c->tvector[c->tncount].division_rank = c->num_divs;
				c->tncount++;
				break;
			case 'P':
				if(check_allocations_exist(cnum) == -1)
				{
					return -1;
				}
				if(c->num_playernames == psize)
				{
					fprintf(stderr, "Allocations and player count does not match!\n");
					return -1;
				}
				c->pvector[c->num_playernames++] = token;
				break;
			default:
				break;
		}
	}

	return cnum++;
}

char *country_playername(int country, int pindex)
{
	return (char *) cvec[country].pvector[pindex];
}

int country_num_playernames(int country)
{
	return cvec[country].num_playernames;
}

int country_num_teams_in_division(int country, int division_rank)
{
	int i;
	int count;
	for(i=0, count=0 ; i<cvec[country].tncount; i++)
	{
		if(cvec[country].tvector[i].division_rank == division_rank)
		{
			count++;
		}
	}
	return count;
}

char *country_division_name(int country, int division_rank)
{
	return (char *) cvec[country].dvector[division_rank-1];
}

int country_num_divisions(int country)
{
	return cvec[country].num_divs;
}

int country_num_teams_total(int country)
{
	return cvec[country].tncount;
}

int country_division_rank(int country, int division_index)
{
	int dc;
	int last;
	last = -1;
	dc = 0;
	while(division_index >= 0)
	{
		if(cvec[country].tvector[dc].division_rank != last)
		{
			division_index--;
		}
		last = cvec[country].tvector[dc].division_rank;
		dc++;
	}

	return last;
}

char *country_team_name_in_division(int country, int division_rank, int team_index)
{
	int i;
	int j;
	int k;

	for(i=0; cvec[country].tvector[i].division_rank != division_rank ; i++)
	{
	}

	k = country_num_teams_in_division(country, division_rank);

	for(j=i ; j<k+i; j++)
	{
		if(team_index-- == 0)
		{
			return cvec[country].tvector[j].name;
		}
	}

	return NULL;
}

char *country_random_player_name(int country)
{
	return cvec[country].pvector[rand() % cvec[country].num_playernames];
}

void country_free()
{
	int i;
	for(i=0 ; i<cnum ; i++)
	{
		free(cvec[i].filebuffer);
		free(cvec[i].pvector);
		free(cvec[i].tvector);
		free(cvec[i].dvector);
	}
	bzero(cvec, sizeof(*cvec)*cnum);
	cnum = 0;
}

#ifdef FTEST_COUNTRY
#include <assert.h>
int main(int argc, char *argv[])
{
	int italy;
	italy = country_read(DATADIR "italy.txt");
	assert(italy == 0);
	assert(cvec[0].num_divs == 2);
	assert(strcmp((char *) cvec[0].pvector[10], "Dante") == 0);
	assert(strcmp(country_playername(italy, 10), "Dante") == 0);
	assert(country_num_playernames(italy) == 324);
	assert(country_num_divisions(italy) == 2);
	assert(country_num_teams_in_division(italy, 1) == 20);
	assert(country_num_teams_in_division(italy, 2) == 22);
	assert(country_num_teams_total(italy) == 42);
	assert(strcmp(country_division_name(italy, 1), "Serie A") == 0);
	assert(strcmp(country_division_name(italy, 2), "Serie B") == 0);
	assert(strcmp(country_team_name_in_division(italy, 1, 0), "Inter") == 0);
	country_free();
	return 0;
}
#endif 

