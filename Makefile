#    dribble - a football manager game
#    Copyright (C) 2008  Jonas Sandberg
#
#    This file is part of dribble.
#
#    dribble is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    dribble is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with dribble.  If not, see <http://www.gnu.org/licenses/>.

DATADIR=\"/usr/share/dribble/\"
PROGNAME=dribble
OBJS=main.o country.o game.o cli.o player.o ref.o
CFLAGS=-Wall -pedantic-errors -Werror -Winline -ansi -DDATADIR=${DATADIR}
LDFLAGS=-lreadline
VALGRIND_FLAGS=-q --leak-check=full --leak-resolution=high

all: ${OBJS} 
	@echo " ld ${PROGNAME}"
	@gcc ${LDFLAGS} ${OBJS} -o ${PROGNAME}

%.o: %.c
	@echo " cc $<"
	@gcc -c ${CFLAGS} $<

clean:
	@rm -f *.o ${PROGNAME} *.test

lrt: 
	@rm -f *.o *.test
	@echo test country.c
	@gcc -o country.c.test -DFTEST_COUNTRY ${CFLAGS} country.c
	@/usr/bin/valgrind ${VALGRIND_FLAGS} ${PWD}/country.c.test
	@echo test player.c
	@gcc -o player.c.test -DFTEST_PLAYER ${CFLAGS} player.c country.c
	@/usr/bin/valgrind ${VALGRIND_FLAGS} ${PWD}/player.c.test
	@echo test team.c
	@gcc -o team.c.test -DFTEST_TEAM ${CFLAGS} team.c player.c country.c
	@/usr/bin/valgrind ${VALGRIND_FLAGS} ${PWD}/team.c.test
	@echo test division.c
	@gcc -o division.c.test -DFTEST_DIVISION ${CFLAGS} division.c team.c player.c country.c
	@/usr/bin/valgrind ${VALGRIND_FLAGS} ${PWD}/division.c.test
	@echo test league.c
	@gcc -o league.c.test -DFTEST_LEAGUE ${CFLAGS} league.c division.c team.c player.c country.c
	@/usr/bin/valgrind ${VALGRIND_FLAGS} ${PWD}/league.c.test
	@echo "OK"
	@rm -f *.o ${PROGNAME} *.test

